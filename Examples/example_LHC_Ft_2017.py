# this is an example that can be launched on lxplus with
# python example_LHC_Ft_2017.py

# it uses the DELPHI_wrapper function and the Impedance library, hence it needs
# the legacy IW2D library (python2), and an impedance file somewhere (see below).

# change the folders, scenario, impedance file names below, as well as lxplusbatchDEL
# into 'launch' (to launch the calculation on HTCondor) or 'retrieve' (to get the results).
# you may also change the various flags (flageigenvect, flat_eta_dipole 
# and flag_eta_crab, in particular), to switch computation of eigenvectors and
# of sensitivities to dipole or crab noise.

import time as ti
import numpy as np
import os,json,warnings
from particle_param import proton_param
from io_lib import write_ncol_file
from string_lib import float_to_str
from Impedance import imp_model_from_file
from DELPHI import DELPHI_wrapper,longdistribution_decomp

warnings.simplefilter('ignore')

electron, m0, clight, E0 = proton_param()
machine = 'LHC'

# result folder
ResultDir = '/afs/cern.ch/work/n/nimounet/private/DELPHI_results/LHC/Test'
ResultsFolderDELPHI = 'DELPHIrun'
scenario = "LHC_Ft2017"
# 'launch', 'retrieve' or None in the following
lxplusbatchDEL = 'launch'

# LHC impedance model (2017, flat top) from file
imp_file = '/afs/cern.ch/user/n/nimounet/private/IRIS/LHC_IW_model/Results/Zxdip_Allthemachine_6p5TeV_B1_LHC_ft_6.5TeV_B1_2017.dat'
imp_mod = imp_model_from_file(imp_file,'Zxdip',ignored_rows=1)


# flags and numerical parameters
flagdamperimp = 0 # 1 to use frequency dependent damper gain (provided in Zd,fd)
d = None
freqd = None
strnorm = ['']
flagnorm = 0 # 1 if damper matrix normalized at current chromaticity (instead of at zero chroma)
nevery = 1 # downsampling of the impedance (take less points than in the full model)
kmax = 1 # number of converged eigenvalues (kmax most unstable ones are converged)
kmaxplot = 20 # number of kept and plotted eigenvalues (in TMCI plot)
flagm0 = True
flageigenvect = False
flag_eta_dipole = False
flag_eta_crab = False
queue = '2nd' # LXBATCH queue (converted in DELPHI to a ht_condor one)
# DELPHI convergence criteria
crit = 5e-3
abseps = 1e-4
nevery = 1

planes = ['x']
dphase = 0.
#Nbscan = np.arange(0.1,10.1,0.1)*1e11 # bunch intensity
Nbscan = np.array([2.e11]) # bunch intensity
Mscan = np.array([3564])
Qpscan = np.array([10.,20.])
dampscan = np.array([0.,0.01])

# deal with optics and parameters
circ=26658.8832 # total circumference in m
R=circ/(2.*np.pi) # machine physical radius
Qx=62.31
Qxfrac=Qx-np.floor(Qx)
Qy=60.32
Qyfrac=Qy-np.floor(Qy)
alphap = 3.48e-4 # momentum compaction factor

E = 6.5e12
gamma=E*electron/E0
beta=np.sqrt(1.-1./(gamma**2))
f0=clight*beta/circ # rev. frequency
omega0=2.*np.pi*f0
eta=alphap-1./(gamma*gamma) # slip factor
print("gamma={}, beta={}".format(gamma,beta))

# longitudinal parameters
taub = 1e-9 # full bunch length
sigmaz = taub*clight*beta/4. # RMS bunch length (m)
Qs = 1.909e-3
typelong = 'Gaussian' # Longitudinal distribution
omegas=Qs*omega0
print("eta={}, Qs={}, taub={}, sigmaz={}".format(eta,Qs,taub,sigmaz))

# Longitudinal distribution decomposition nad average beta functions used
g, a, b = longdistribution_decomp(taub, typelong=typelong)
avbetax = R/Qx
avbetay = R/Qy

print(os.path.join(scenario,ResultsFolderDELPHI))

# Initialize tune shifts
tuneshiftQp = np.zeros((len(planes),len(Mscan),len(Qpscan),len(dampscan),len(Nbscan),1,1,kmaxplot), dtype=complex)
tuneshiftm0Qp = np.zeros((len(planes),len(Mscan),len(Qpscan),len(dampscan),len(Nbscan),1,1), dtype=complex)

print('DELPHI, LHC test computation')
result_folder = os.path.join(ResultDir,scenario,ResultsFolderDELPHI)
os.system("mkdir -p "+result_folder)

# DELPHI run
if lxplusbatchDEL in ['launch',None]:

    DELPHI_params = {'Mscan': Mscan.tolist(),
                     'Qpscan': Qpscan.tolist(),
                     'dampscan': dampscan.tolist(),
                     'Nbscan': Nbscan.tolist(),
                     'omegasscan': [omegas],
                     'dphasescan': [dphase],
                     'omega0': omega0,
                     'Qx': Qx, 'Qy': Qy,
                     'gamma': gamma, 'eta': eta,
                     'a': a,'b': b,
                     'taub': taub, 'g': g.tolist(),
                     'planes': planes,
                     'nevery': nevery,
                     'particle': 'proton',
                     'flagnorm': flagnorm,'flagdamperimp': flagdamperimp,
                     'd': d, 'freqd': freqd,
                     'kmax': kmax, 'kmaxplot': kmaxplot,
                     'crit': crit, 'abseps': abseps, 
                     'flagm0': flagm0, 'flageigenvect': flageigenvect,
                     'flag_eta_dipole': flag_eta_dipole, 'flag_eta_crab': flag_eta_crab,
                     'lxplusbatch': lxplusbatchDEL,
                     'comment': "{}_{}".format(machine,scenario),
                     'queue': queue, 'dire': result_folder+'/',
                     'flagQpscan_outside': True,
                     }

    with open(os.path.join(result_folder,'DELPHI_parameters.json'), 'w') as f:
        f.write(json.dumps(DELPHI_params,indent=4))

if lxplusbatchDEL!='restore':
    t0 = ti.time()
    result_dict = DELPHI_wrapper(imp_mod, Mscan, Qpscan, dampscan, Nbscan,
                          [omegas], [dphase], omega0, Qx, Qy, gamma, eta, a, b, taub, g,
                          planes, nevery=nevery, particle='proton', flagnorm=flagnorm,
                          flagdamperimp=flagdamperimp, d=d, freqd=freqd, kmax=kmax, kmaxplot=kmaxplot,
                          crit=crit, abseps=abseps, flagm0=flagm0, flageigenvect=flageigenvect,
                          flag_eta_dipole=flag_eta_dipole, flag_eta_crab=flag_eta_crab,
                          lxplusbatch=lxplusbatchDEL, comment="{}_{}".format(machine,scenario),
                          queue=queue, dire=result_folder+'/', flagQpscan_outside=True)
    tuneshiftQp = result_dict['tuneshiftQp']
    tuneshiftm0Qp = result_dict['tuneshiftm0Qp']
    if flag_eta_dipole:
        eta_dipole = result_dict['eta_dipole']
    if flag_eta_crab:
        eta_crab = result_dict['eta_crab']
    t1 = ti.time()
    print("Elapsed time={} s".format(t1-t0))

else:
    tuneshiftQp[:,:,:,:,:,:,:,:] = np.load(os.path.join(result_folder,'TuneshiftQp.npy') )
    tuneshiftm0Qp[:,:,:,:,:,:,:] = np.load(os.path.join(result_folder,'Tuneshiftm0Qp.npy') )

if lxplusbatchDEL in ['retrieve',None,'restore']:
    # Output files name for data vs Qp
    E = gamma*E0/electron
    Estr = float_to_str(round(E/1e9))+'GeV'
    with open(os.path.join(result_folder,'Qs.txt'),'w') as f:
        f.write("{}\n".format(Qs))
    np.save(os.path.join(result_folder,'TuneshiftQp.npy'), tuneshiftQp[:,:,:,:,:,:,:,:])
    np.save(os.path.join(result_folder,'Tuneshiftm0Qp.npy'), tuneshiftm0Qp[:,:,:,:,:,:,:])

    strpart=['Re','Im']

    for iplane, plane in enumerate(planes):
        for iM, M in enumerate(Mscan):
            for idamp,damp in enumerate(dampscan):

                print("M={}, d={}, plane={}".format(M,damp,plane))
                for Nb in Nbscan:

                    for ir, r in enumerate(['real','imag']):

                        # Output files name for data vs Qp
                        fileoutdataQp = os.path.join(result_folder,
                            'data_vs_Qp_'+machine+'_'+Estr+scenario+'_'+str(M)+'b_d'+float_to_str(damp)+'_Nb'+float_to_str(Nb/1.e11)+'e11_converged'+strnorm[flagnorm]+'_'+plane)
                        fileoutdataQpm0 = os.path.join(result_folder,
                            'data_vs_Qp_m0_'+machine+'_'+Estr+scenario+'_'+str(M)+'b_d'+float_to_str(damp)+'_Nb'+float_to_str(Nb/1.e11)+'e11_converged'+strnorm[flagnorm]+'_'+plane)
                        fileoutdata_all = os.path.join(result_folder,
                            'data_vs_Qp_all_'+machine+'_'+Estr+scenario+'_'+str(M)+'b_d'+float_to_str(damp)+'_Nb'+float_to_str(Nb/1.e11)+'e11_converged'+strnorm[flagnorm]+'_'+plane)

                        ts = getattr(tuneshiftQp[iplane,iM,:,idamp,np.where(Nbscan==Nb),0,0,0], r)
                        data = np.hstack((Qpscan.reshape((-1,1)), ts.reshape((-1,1))))
                        write_ncol_file(fileoutdataQp+'_'+r+'.dat', data, header="Qp\t"+strpart[ir]+"_tuneshift")

                        tsm0 = getattr(tuneshiftm0Qp[iplane,iM,:,idamp,np.where(Nbscan==Nb),0,0], r)
                        data = np.hstack((Qpscan.reshape((-1,1)), ts.reshape((-1,1))))
                        write_ncol_file(fileoutdataQpm0+'_'+r+'.dat', data, header="Qp\t"+strpart[ir]+"_tuneshiftm0")

                        all_unstable_modes = getattr(tuneshiftQp[iplane,iM,:,idamp,np.where(Nbscan==Nb),0,0,:], r)
                        data = np.hstack((Qpscan.reshape((-1,1)), all_unstable_modes.reshape((-1,kmaxplot))))
                        write_ncol_file(fileoutdata_all+'_'+r+'.dat', data, header="Qp\t"+strpart[ir]+"_tuneshift")

                    if flag_eta_dipole:
                        fileoutdata_eta_dipole = os.path.join(result_folder,
                            'data_vs_Qp_eta_dipole_most_unstable_coupledbunch_'+machine+'_'+Estr+scenario+'_'+str(M)+'b_d'+float_to_str(damp)+'_Nb'+float_to_str(Nb/1.e11)+'e11_converged'+strnorm[flagnorm]+'_'+plane)
                        data = np.hstack((Qpscan.reshape((-1,1)), eta_dipole[iplane,iM,:,0,idamp,np.where(Nbscan==Nb),0,0,:].reshape((-1,kmaxplot))))
                        write_ncol_file(fileoutdata_eta_dipole+'.dat', data, header="Qp\t"+"eta_dipole")
                        fileoutdata_eta_dipole = os.path.join(result_folder,
                            'data_vs_Qp_eta_dipole_most_unstable_radial_'+machine+'_'+Estr+scenario+'_'+str(M)+'b_d'+float_to_str(damp)+'_Nb'+float_to_str(Nb/1.e11)+'e11_converged'+strnorm[flagnorm]+'_'+plane)
                        data = np.hstack((Qpscan.reshape((-1,1)), eta_dipole[iplane,iM,:,:,idamp,np.where(Nbscan==Nb),0,0,0].reshape((len(Qpscan),-1))))
                        write_ncol_file(fileoutdata_eta_dipole+'.dat', data, header="Qp\t"+"eta_dipole")

                    if flag_eta_crab:
                        fileoutdata_eta_crab = os.path.join(result_folder,
                            'data_vs_Qp_eta_crab_most_unstable_coupledbunch_'+machine+'_'+Estr+scenario+'_'+str(M)+'b_d'+float_to_str(damp)+'_Nb'+float_to_str(Nb/1.e11)+'e11_converged'+strnorm[flagnorm]+'_'+plane)
                        data = np.hstack((Qpscan.reshape((-1,1)), eta_crab[iplane,iM,:,0,idamp,np.where(Nbscan==Nb),0,0,:].reshape((-1,kmaxplot))))
                        write_ncol_file(fileoutdata_eta_crab+'.dat', data, header="Qp\t"+"eta_crab")
                        fileoutdata_eta_dipole = os.path.join(result_folder,
                            'data_vs_Qp_eta_crab_most_unstable_radial_'+machine+'_'+Estr+scenario+'_'+str(M)+'b_d'+float_to_str(damp)+'_Nb'+float_to_str(Nb/1.e11)+'e11_converged'+strnorm[flagnorm]+'_'+plane)
                        data = np.hstack((Qpscan.reshape((-1,1)), eta_dipole[iplane,iM,:,:,idamp,np.where(Nbscan==Nb),0,0,0].reshape((len(Qpscan),-1))))
                        write_ncol_file(fileoutdata_eta_dipole+'.dat', data, header="Qp\t"+"eta_crab")
