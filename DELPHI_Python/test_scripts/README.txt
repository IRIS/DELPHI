# test_noise.py is testing the eta computation, comparing it with an
# approximated formula - see S. Furuseth and X. Buffat, EPJP 137, 506 (2022)
# to use it:
python test_noise.py

# the first resulting plot should show a perfect agreement between the curves
 # (full computation) and the crosses (approximation)

