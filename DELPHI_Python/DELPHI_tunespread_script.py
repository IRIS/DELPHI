import os,sys
import subprocess
# import local libraries if needed
pymod=subprocess.check_output("echo $PYMOD",shell=True).strip().decode()
if pymod.startswith('local'):
    py_numpy=subprocess.check_output("echo $PY_NUMPY",shell=True).strip().decode()
    sys.path.insert(1,py_numpy)
    py_scipy=subprocess.check_output("echo $PY_SCIPY",shell=True).strip().decode()
    sys.path.insert(1,py_scipy)
    py_matpl=subprocess.check_output("echo $PY_MATPL",shell=True).strip().decode()
    sys.path.insert(1,py_matpl)
from string import *
from DELPHI import *
from DELPHI_script import extract_scalar_from_array
import pickle as pick

# script encapsulating a DELPHI calculation (with tunespread) with scans
# launched in command line with one argument, the name of the file containing
# all the parameters (pickle format)


if __name__ == "__main__":

    if len(sys.argv)>1:
        filename=str(sys.argv[1])
    print(filename)

    listargs = ['Qpscan', 'nxscan', 'dampscan', 'Nbscan', 'omegasscan', 'dphasescan',
                'bxscan','bxyscan','M','omega0', 'Q', 'gamma', 'eta', 'a', 'b', 'taub', 'g', 'Z', 'freq']
    param_dict = dict(np.load(filename,allow_pickle=True))
    print("Parameters:")

    for key,value in param_dict.items():
        if key in listargs:
            exec("{}=extract_scalar_from_array(value)".format(key))
            print("{} = {}".format(key,eval(key)))
        else:
            param_dict[key] = extract_scalar_from_array(value)
    for key in listargs:
        param_dict.pop(key)
    pprint(param_dict)

    # remove initial file
    os.system("rm -f "+filename)

    flagm0 = param_dict.get('flagm0',False)

    if flagm0:
        tuneshift_most,tuneshiftnx,tuneshiftm0=eigenmodesDELPHI_tunespread_converged_scan(
            Qpscan,nxscan,dampscan,Nbscan,omegasscan,dphasescan,bxscan,bxyscan,M,
            omega0,Q,gamma,eta,a,b,taub,g,Z,freq, **param_dict)
    else:
        tuneshift_most,tuneshiftnx=eigenmodesDELPHI_tunespread_converged_scan(
            Qpscan,nxscan,dampscan,Nbscan,omegasscan,dphasescan,bxscan,bxyscan,M,
            omega0,Q,gamma,eta,a,b,taub,g,Z,freq, **param_dict)

    print(tuneshift_most.shape,tuneshiftnx.shape)
    print(tuneshift_most)#,tuneshiftnx)
    if flagm0:
        print(tuneshiftm0.shape,tuneshiftm0)

    tuneshiftnx=[] # 7/12/2013: I erase it because it is never used... (to save space in multibunch)
    # save results
    with open('out'+filename,'wb') as fileall:
        pick.dump(tuneshift_most,fileall)
        pick.dump(tuneshiftnx,fileall)
        if flagm0:
            pick.dump(tuneshiftm0,fileall)
