Qp= -3.1098 , M= 1 , d= 0.0 , Nb= 4e+12 , omegas= 10566.9792167 , dphase= 0.0
kmode= 0 , Most unstable coupled-bunch mode:  0
Qp= -3.1171 , M= 1 , d= 0.0 , Nb= 4e+12 , omegas= 10566.9792167 , dphase= 0.0
kmode= 0 , Most unstable coupled-bunch mode:  0
Qp= -3.1244 , M= 1 , d= 0.0 , Nb= 4e+12 , omegas= 10566.9792167 , dphase= 0.0
kmode= 0 , Most unstable coupled-bunch mode:  0
**************************************************
PSB HOM test: elapsed time = 105.986047983 seconds
**************************************************
Qp= -15 , M= 3564 , d= 0.0 , Nb= 2.3e+11 , omegas= 148.380869001 , dphase= 0.0
kmode= 0 , Most unstable coupled-bunch mode:  3018
Qp= -15 , M= 3564 , d= 0.02 , Nb= 2.3e+11 , omegas= 148.380869001 , dphase= 0.0
kmode= 0 , Most unstable coupled-bunch mode:  3018
Qp= 10 , M= 3564 , d= 0.0 , Nb= 2.3e+11 , omegas= 148.380869001 , dphase= 0.0
kmode= 0 , Most unstable coupled-bunch mode:  3018
Qp= 10 , M= 3564 , d= 0.02 , Nb= 2.3e+11 , omegas= 148.380869001 , dphase= 0.0
kmode= 0 , Most unstable coupled-bunch mode:  3018
**************************************************
HL-LHC HOM test: elapsed time = 64.3095269203 seconds
**************************************************
Qp= -10 , M= 1 , d= 0.0 , Nb= 6e+11 , omegas= 134.885275484 , dphase= 0.0
kmode= 0 , Most unstable coupled-bunch mode:  0
Qp= -10 , M= 1 , d= 0.01 , Nb= 6e+11 , omegas= 134.885275484 , dphase= 0.0
kmode= 0 , Most unstable coupled-bunch mode:  0
Qp= 0 , M= 1 , d= 0.0 , Nb= 6e+11 , omegas= 134.885275484 , dphase= 0.0
kmode= 0 , Most unstable coupled-bunch mode:  0
Qp= 0 , M= 1 , d= 0.01 , Nb= 6e+11 , omegas= 134.885275484 , dphase= 0.0
kmode= 0 , Most unstable coupled-bunch mode:  0
Qp= 10 , M= 1 , d= 0.0 , Nb= 6e+11 , omegas= 134.885275484 , dphase= 0.0
kmode= 0 , Most unstable coupled-bunch mode:  0
Qp= 10 , M= 1 , d= 0.01 , Nb= 6e+11 , omegas= 134.885275484 , dphase= 0.0
kmode= 0 , Most unstable coupled-bunch mode:  0
**************************************************
LHC RW test: elapsed time = 238.010614872 seconds
**************************************************
Qp= 0 , M= 1 , d= 0.0 , Nb= 3.8e+11 , omegas= 134.875275923 , dphase= 0.0
kmode= 0 , Most unstable coupled-bunch mode:  0
Qp= 0 , M= 1 , d= 0.0 , Nb= 3.8e+11 , omegas= 134.875275923 , dphase= 0.0
kmode= 0 , coupled-bunch mode with highest tune shift:  0
**************************************************
LHC BB TMCI test: elapsed time = 6.14798808098 seconds
**************************************************
LHC BB matrix test: elapsed time = 109.306926012 seconds
**************************************************
Qp= -5 , M= 11 , d= 0.0 , Nb= 100000000.0 , omegas= 148.380869001 , dphase= 0.0
kmode= 0 , Most unstable coupled-bunch mode:  3
Qp= 10 , M= 11 , d= 0.0 , Nb= 100000000.0 , omegas= 148.380869001 , dphase= 0.0
kmode= 0 , Most unstable coupled-bunch mode:  7
**************************************************
LHC BB eta test: elapsed time = 25.2356050014 seconds
**************************************************
All tests passed with success.
**************************************************
