from particle_param import proton_param
from DELPHI import Qs_from_RF_param,longdistribution_decomp,eigenmodesDELPHI_converged_scan
from DELPHI import damper_imp_Karliner_Popov,compute_impedance_matrix,compute_damper_matrix
import os,time
import numpy as np
from scipy.special import ive

# proton parameters
electron, m0, clight, E0 = proton_param()

# some general flags and miscellaneous
flagnorm = 0 # 1 if damper matrix normalized at current chromaticity (instead of at zero chroma)
mu0 = 4e-7*np.pi
Z0 = mu0*clight
eps0 = 1./(mu0*clight**2)

# DELPHI convergence criteria
crit = 5e-2
abseps = 1e-4

def resonator_impedance(R,fr,Q,freq):
    '''
    Resonator model impedance (transverse)
    R=shunt impedance (Ohm/m), fr=resonant frequency, Q=quality factor
    computes impedance at the frequencies freq (2 columns array)
    '''
    Z = np.zeros((len(freq),2))
    Zt = (R*fr/freq)/(1.-1j*Q*(fr/freq-freq/fr))
    Z[:,0] = np.real(Zt)
    Z[:,1] = np.imag(Zt)

    return Z

def classic_thick_wall_formula(omega):
    # classic thick wall formula (see e.g. Chao's book on collective effects)
    delta_skin = np.sqrt(2.*rho/(mu0*omega))
    Zdip = circ*Z0*delta_skin/(2.*np.pi*radius**3)*beta
    return np.hstack((Zdip.reshape(-1,1),Zdip.reshape(-1,1)))

def single_layer_formula(omega):
    # single-layer formula, as in F. Roncarolo et al, PRST-AB (2009), or
    # N. Mounet PhD's thesis (https://infoscience.epfl.ch/record/174672)
    # with alpha_TM from CERN-ACC-NOTE-2015-0017 p. 10
    from scipy import special as sp
    tau = 0.027e-12 # relaxation time of Cu, in seconds
    sigma = 1./(rho*(1.+1j*omega*tau)) # AC conductivity
    eps1 = 1. + sigma/(1j*eps0*omega)
    ups = beta*clight
    k = omega/ups
    nu = k*np.sqrt(1-beta**2*eps1)
    x1 = k*radius/gamma
    x2 = nu*radius
    # modified Bessel functions in x1
    K1x1 = sp.k1(x1)
    I1x1 = sp.i1(x1)
    ratioI = x2*sp.ivp(1,x1)/(x1*I1x1)
    ratioK = -(sp.kve(0,x2)/sp.kve(1,x2)+1./x2)
    alpha_TM = K1x1/I1x1 + (x2**2/(x1**2*I1x1**2))/((1-x2**2/x1**2)**2/(x2*beta**2*(ratioI-ratioK))
                                                    - x2*(ratioI-eps1*ratioK))
    Zdip = 1j*k**2*Z0*circ/(4*np.pi*beta*gamma**4)*alpha_TM
    return np.hstack((np.real(Zdip).reshape(-1,1),np.imag(Zdip).reshape(-1,1)))

#######################################################
# first test: HOM in PSBooster with tune/chroma scan
#######################################################

machine = 'PSB'

# parameter definitions
lmax = 13
nmax = 24
plane = 'x'
dphase = 0.
M = 1
Nb = 4e12
damp = 0.
kmaxplot = 1

nxscan = [0]
Qxscan = [4.26, 4.27, 4.28]
# expected tuneshifts from 2020 PSB calculations by N. Mounet on local machine
# (notebook ~/cernbox/Notebooks/PSB_HOM_from_Eirini_inputs.ipynb, results in 
# ~/cernbox/Simulations/DELPHI/PSB/HOM/Eirini_new_DELPHI_parabolicline/) 
expected_dQs = [(0.00521218799685-0.000218390857305j),
                (-0.00516782662064-0.000276960638072j),
                (-0.00522015556111-8.9741581243e-05j)]

# machine and beam parameters
circ = 157.07
R = circ/(2.*np.pi) # machine radius
Ekin = 160e6
gamma = Ekin*electron/E0 + 1
beta=np.sqrt(1.-1./(gamma**2))
f0=clight*beta/circ # rev. frequency
omega0=2.*np.pi*f0

# optics
alphap = 0.06016
#print("gamma={}, frev={}, alphap={}".format(gamma,f0,alphap))

# narrow-band impedance (HOM)
Rt = 4e6 # shunt impedance in MOhm/m
fr = 1.72e6 # cutoff frequency in Hz
Q = 100 # quality factor
freq = np.hstack(([1e-5],np.arange(5e2,5e6+5e2,5e2)))
#freq = np.sort(list(set(np.hstack((np.logspace(1,13,121),np.linspace(1e9,1e10,201))))))
Z = resonator_impedance(Rt,fr,Q,freq)

# longitudinal parameters
h = 1 # harmonic number
V = 8e3 # RF voltage
taub = 503e-9 # bunch length (sec)
sigmaz = taub*beta*clight/4. # RMS bunch length (m)
typelong = 'parabolicline' # or 'Gaussian' (Longitudinal distribution)
eta = alphap-1./(gamma*gamma) # slip factor
#Qs = 0.0016953
#omegas = Qs*omega0
omegas = 10566.979216700205

t0 = time.time()
#print("eta={}, Qs={}, sigmaz={}, omegas={}".format(eta,Qs,sigmaz,omegas))
# Longitudinal distribution decomposition
g, a, b = longdistribution_decomp(taub, typelong=typelong)

for Qx,expected_dQ in zip(Qxscan,expected_dQs):
    # DELPHI launch
    Qp = -0.73*Qx
    result_dict = eigenmodesDELPHI_converged_scan(
            [Qp], nxscan, [damp], [Nb], [omegas], [dphase], M,
            omega0, Qx, gamma, eta, a, b, taub, g, Z, freq,
            particle='proton', flagnorm=flagnorm, flag_trapz=0, flagdamperimp=0,
            d=None, freqd=None, kmax=1, kmaxplot=kmaxplot, crit=crit, abseps=abseps,
            flagm0=False, flageigenvect=False,optimized_convergence=True,
            fixed_lmax_nmax=(lmax,nmax))
    tuneshift_most = result_dict['tuneshift_most']
    tuneshiftnx = result_dict['tuneshiftnx']

    np.testing.assert_array_equal(tuneshift_most.shape,(1, 1, 1, 1, 1, kmaxplot),
                                  err_msg="Qx={}: tuneshift_most has incorrect shape".format(Qx))
    np.testing.assert_array_equal(tuneshiftnx.shape,(1, 1, 1, 1, 1, 1, kmaxplot),
                                  err_msg="Qx={}: tuneshiftnx has incorrect shape".format(Qx))
    
    assert not np.any(np.isnan(tuneshift_most)), "Qx={}: {} nan found".format(Qx,len(np.any(np.isnan(tuneshift_most))))
    assert not np.all(tuneshift_most==0.), "Qx={}: only zeros found".format(Qx)

    np.testing.assert_allclose(np.real(tuneshiftnx.squeeze()),np.real(tuneshift_most.squeeze()),
                               rtol=0.,atol=1e-15,err_msg="Qx={}: tuneshift_most and tuneshiftnx differ (real part)".format(Qx))
    np.testing.assert_allclose(np.imag(tuneshiftnx.squeeze()),np.imag(tuneshift_most.squeeze()),
                               rtol=0.,atol=1e-15,err_msg="Qx={}: tuneshift_most and tuneshiftnx differ (imag part)".format(Qx))
    np.testing.assert_allclose(np.real(tuneshift_most[0,0,0,0,0,0]),np.real(expected_dQ),
                               rtol=1e-5,atol=0.,
                               err_msg="Qx={}: real(tuneshift_most) not as expected, diff.={}".format(
                                Qx, np.real(tuneshift_most[0,0,0,0,0,0])-np.real(expected_dQ)))
    np.testing.assert_allclose(np.imag(tuneshift_most[0,0,0,0,0,0]),np.imag(expected_dQ),
                               rtol=1e-5,atol=0.,
                               err_msg="Qx={}: imag(tuneshift_most) not as expected, diff.={}".format(
                                Qx, np.imag(tuneshift_most[0,0,0,0,0,0])-np.imag(expected_dQ)))

print("*"*50)
print("PSB HOM test: elapsed time = {} seconds".format(time.time()-t0))
print("*"*50)


##################################################################
# second test: HOM in HL-LHC with chroma and damper gain scans
##################################################################

machine = 'LHC'

# parameter definitions
plane = 'x'
dphase = 0.
M = 3564
Nb = 2.3e11
dampscan = [0.,0.02]

nxscan = np.array([0,3017,3018,3019,M-1])
Qx = 62.3099999992371
Qpscan = [-15, 10]
# expected tuneshifts from 2019 HLLHC HOM calculations by N. Mounet on lxplus
# (notebook /afs/cern.ch/user/n/nimounet/private/IRIS/LHC_IW_model/Python-scripts/HOM_HLLHC_test.ipynb,
# results in /afs/cern.ch/work/n/nimounet/private/DELPHI_results/HLLHC/HOM_test/HOM_opt_conv_DELPHI/DELPHIrun/)
expected_dQs = {-15: [(3.6637946556e-06 - 2.2384339467e-05*1j),
                      (3.5854917725e-06 - 2.1762100826e-05*1j),
                      ],
                10: [(2.3640172518e-06 - 1.4026337348e-05*1j),
                     (2.3640351669e-06 - 1.4026331515e-05*1j),
                     ],
                }

# machine and beam parameters
circ = 26658.8831999989
R = circ/(2.*np.pi) # machine radius
gamma = 7460.52252761119
beta=np.sqrt(1.-1./(gamma**2))
f0=clight*beta/circ # rev. frequency
omega0=2.*np.pi*f0

# optics
alphap = 0.000346334442089481
#print("gamma={}, frev={}, alphap={}".format(gamma,f0,alphap))

# narrow-band impedance (HOM)
Rt = 2.7e6*52 # shunt impedance in MOhm/m (52 is for the beta function factor at beta*=15cm)
fr = 1.97e9 # HOM frequency in Hz
Q = 3.1e4 # quality factor
freq = np.sort(list(set(np.hstack((np.logspace(1,13,121),np.linspace(1.96e9,1.98e9,5001))))))
Z = resonator_impedance(Rt,fr,Q,freq)

# longitudinal parameters
taub = 1.2008307535006611e-09 # bunch length (sec)
sigmaz = taub*beta*clight/4. # RMS bunch length (m)
typelong = 'Gaussian' # Longitudinal distribution
eta = alphap-1./(gamma*gamma) # slip factor
omegas = 148.38086900107388

t0 = time.time()
#print("eta={}, sigmaz={}, omegas={}".format(eta,sigmaz,omegas))
# Longitudinal distribution decomposition
g, a, b = longdistribution_decomp(taub, typelong=typelong)

for Qp in Qpscan:
    expected_dQ = np.array(expected_dQs[Qp])
    # DELPHI launch
    result_dict = eigenmodesDELPHI_converged_scan(
            [Qp], nxscan, dampscan, [Nb], [omegas], [dphase], M,
            omega0, Qx, gamma, eta, a, b, taub, g, Z, freq,
            particle='proton', flagnorm=flagnorm, flag_trapz=0, flagdamperimp=0,
            d=None, freqd=None, kmax=1, kmaxplot=1, crit=crit, abseps=abseps,
            flagm0=False, flageigenvect=False, optimized_convergence=True,
            )
    tuneshift_most = result_dict['tuneshift_most']
    tuneshiftnx = result_dict['tuneshiftnx']

    np.testing.assert_array_equal(tuneshift_most.shape,(1, 2, 1, 1, 1, 1),
                                  err_msg="Qp={}: tuneshift_most has incorrect shape".format(Qp))
    np.testing.assert_array_equal(tuneshiftnx.shape,(1, len(nxscan), 2, 1, 1, 1, 1),
                                  err_msg="Qp={}: tuneshiftnx has incorrect shape".format(Qp))
    
    assert not np.any(np.isnan(tuneshiftnx)), "Qp={}: {} nan found".format(Qp,len(np.any(np.isnan(tuneshiftnx))))
    assert not np.all(tuneshiftnx==0.), "Qp={}: only zeros found".format(Qp)
    
    nxmin = [np.argmin(np.imag(tuneshiftnx[0,:,idamp,0,0,0,0])) for idamp,_ in enumerate(dampscan)]
    tuneshiftnx_min = [tuneshiftnx[0,nx,inx,0,0,0,0] for inx,nx in enumerate(nxmin)]
    
    np.testing.assert_allclose(np.real(tuneshiftnx_min),np.real(tuneshift_most.squeeze()),
                               rtol=0.,atol=1e-15,err_msg="Qp={}: tuneshift_most and most unstable mode in tuneshiftnx differ (real part)".format(Qp))
    np.testing.assert_allclose(np.imag(tuneshiftnx_min),np.imag(tuneshift_most.squeeze()),
                               rtol=0.,atol=1e-15,err_msg="Qp={}: tuneshift_most and most unstable mode in tuneshiftnx differ (imag part)".format(Qp))
    np.testing.assert_allclose(np.real(tuneshift_most.squeeze()),np.real(expected_dQ),
                               rtol=1e-5,atol=0.,
                               err_msg="Qp={}: real(tuneshift_most) not as expected, diff.={}".format(
                                Qp, np.real(tuneshift_most.squeeze())-np.real(expected_dQ)))
    np.testing.assert_allclose(np.imag(tuneshift_most.squeeze()),np.imag(expected_dQ),
                               rtol=1e-5,atol=0.,
                               err_msg="Qp={}: imag(tuneshift_most) not as expected, diff.={}".format(
                                Qp, np.imag(tuneshift_most.squeeze())-np.imag(expected_dQ)))

print("*"*50)
print("HL-LHC HOM test: elapsed time = {} seconds".format(time.time()-t0))
print("*"*50)

###################################################################
# third test: RW (classic) in LHC with chroma and damp gain scans
###################################################################

machine = 'LHC'

# parameter definitions
plane = 'x'
dphase = 0.
M = 1
Nb = 6e11
dampscan = [0.,0.01]

nxscan = np.arange(M)
Qx = 62.31
Qpscan = [-10, 0, 10]
# expected tuneshifts from 2019 LHC RW calculations by N. Mounet on lxplus
# (notebook /afs/cern.ch/user/n/nimounet/private/IRIS/LHC_IW_model/Python-scripts/RW_LHC_test_David.ipynb,
# results in /afs/cern.ch/work/n/nimounet/private/DELPHI_results/LHC/Test_David/RW_Qpscan_new_DELPHI/DELPHIrun/)
expected_dQs = {-10: [(-6.9827964188e-04 - 2.3132225523e-04*1j),
                      (-1.3257426535e-06 - 1.5184971746e-08*1j),
                      ],
                0: [(-2.0131300716e-03 - 1.7214005812e-08*1j),
                    (-2.0288465947e-03 - 1.3105455209e-05*1j),
                    ],
                10: [(-2.0652008780e-03 - 4.8257942928e-05*1j),
                     (-9.1595797542e-05 - 9.3889533661e-06*1j),
                     ],
                }

# machine and beam parameters
circ = 26658.8831999989
R = circ/(2.*np.pi) # machine radius
E = 6.5e12
gamma=E*electron/E0
beta=np.sqrt(1.-1./(gamma**2))
f0=clight*beta/circ # rev. frequency
omega0=2.*np.pi*f0

# optics
alphap = 3.48e-4
#print("gamma={}, frev={}, alphap={}".format(gamma,f0,alphap))

freq = 10.**np.arange(-1,15.02,0.02)
omega = 2.*np.pi*freq
rho = 1.7e-8 # resistivity of Cu at 300K, in Ohm.m
radius = 0.01 # beam pipe radius (assumed to be of infinite thickness)

Z = classic_thick_wall_formula(omega)
# longitudinal parameters
taub = 1e-9 # bunch length (sec)
sigmaz = taub*beta*clight/4. # RMS bunch length (m)
typelong = 'Gaussian' # Longitudinal distribution
eta = alphap-1./(gamma*gamma) # slip factor
Qs = 1.909e-3
omegas=Qs*omega0

t0 = time.time()
#print("eta={}, sigmaz={}, omegas={}".format(eta,sigmaz,omegas))
# Longitudinal distribution decomposition
g, a, b = longdistribution_decomp(taub, typelong=typelong)

for Qp in Qpscan:
    expected_dQ = expected_dQs[Qp]
    # DELPHI launch
    result_dict = eigenmodesDELPHI_converged_scan(
            [Qp], nxscan, dampscan, [Nb], [omegas], [dphase], M,
            omega0, Qx, gamma, eta, a, b, taub, g, Z, freq,
            particle='proton', flagnorm=flagnorm, flag_trapz=0, flagdamperimp=0,
            d=None, freqd=None, kmax=1, kmaxplot=1, crit=crit, abseps=abseps,
            flagm0=False, flageigenvect=False, optimized_convergence=True,
            )
    tuneshift_most = result_dict['tuneshift_most']
    tuneshiftnx = result_dict['tuneshiftnx']

    np.testing.assert_array_equal(tuneshift_most.shape,(1, 2, 1, 1, 1, 1),
                                  err_msg="Qp={}: tuneshift_most has incorrect shape".format(Qp))
    np.testing.assert_array_equal(tuneshiftnx.shape,(1, 1, 2, 1, 1, 1, 1),
                                  err_msg="Qp={}: tuneshiftnx has incorrect shape".format(Qp))
    
    assert not np.any(np.isnan(tuneshiftnx)), "Qp={}: {} nan found".format(Qp,len(np.any(np.isnan(tuneshiftnx))))
    assert not np.all(tuneshiftnx==0.), "Qp={}: only zeros found".format(Qp)
    
    np.testing.assert_allclose(np.real(tuneshiftnx.squeeze()),np.real(tuneshift_most.squeeze()),
                               rtol=0.,atol=1e-15,err_msg="Qp={}: tuneshift_most and tuneshiftnx differ (real part)".format(Qp))
    np.testing.assert_allclose(np.imag(tuneshiftnx.squeeze()),np.imag(tuneshift_most.squeeze()),
                               rtol=0.,atol=1e-15,err_msg="Qp={}: tuneshift_most and tuneshiftnx differ (imag part)".format(Qp))
    np.testing.assert_allclose(np.real(tuneshift_most.squeeze())[:1],np.real(expected_dQ)[:1],
                               rtol=2e-3,atol=2e-6,
                               err_msg="Qp={}: real(tuneshift_most) not as expected, diff.={}".format(
                                Qp, np.real(tuneshift_most.squeeze())-np.real(expected_dQ)))
    np.testing.assert_allclose(np.imag(tuneshift_most.squeeze()),np.imag(expected_dQ),
                               rtol=1e-5,atol=1e-6,
                               err_msg="Qp={}: imag(tuneshift_most) not as expected, diff.={}".format(
                                Qp, np.imag(tuneshift_most.squeeze())-np.imag(expected_dQ)))

print("*"*50)
print("LHC RW test: elapsed time = {} seconds".format(time.time()-t0))
print("*"*50)

#######################################################
# fourth test: TMCI in LHC with broadband impedance
#######################################################

machine = 'LHC'

# parameter definitions
plane = 'x'
dphase = 0.
M = 1
Nbscan = [3.8e11]#[4.9e11, 5e11]
#Nbscan = np.arange(1.e10,8.1e11,1e10)
dampscan = [0.]

nxscan = np.arange(M)
Qx = 62.3099997613612
Qp = 0
# expected tuneshifts from 2019 LHC BB TMCI calculations by N. Mounet on local machine
# (maybe using a slightly older version of DELPHI)
# (notebook ~/cernbox/Notebooks/TMCI_broadband_as_Elias_GALACTIC.ipynb,
# results in /afs/cern.ch/work/n/nimounet/private/DELPHI_results/LHC_BB/TMCI/TMCI_with_BB_25MOhm_0p8GHz/DELPHIrun)
# NOTE: these calculations were done with flag_trapz=1 so are slightly different
expected_dQs = {3.8e11:  [(-1.5085252397e-03 - 1.1486943153e-04*1j),
                         ],
                }

# machine and beam parameters
gamma=6927.62806135325
beta=np.sqrt(1.-1./(gamma**2))
omega0=70657.55656577148

# Broad-band impedance
Rt = 25e6 # shunt impedance in MOhm/m
fr = 0.8e9 # cutoff frequency in Hz
Q = 1 # quality factor
freq = np.sort(list(set(np.hstack((np.logspace(1,13,121),np.linspace(5e8,5e9,201))))))
Z = resonator_impedance(Rt,fr,Q,freq)

# longitudinal parameters
taub = 1e-9 # bunch length (sec)
typelong = 'Gaussian' # Longitudinal distribution
eta = 0.0003479538899641484 # slip factor
omegas = 134.87527592319398

t0 = time.time()
# Longitudinal distribution decomposition
g, a, b = longdistribution_decomp(taub, typelong=typelong)
#print("g[0]={}, a={}, b={}".format(g[0],a,b))

if True:
    # DELPHI launch
    result_dict = eigenmodesDELPHI_converged_scan(
            [Qp], nxscan, dampscan, Nbscan, [omegas], [dphase], M,
            omega0, Qx, gamma, eta, a, b, taub, g, Z, freq,
            particle='proton', flagnorm=flagnorm, flag_trapz=0, flagdamperimp=0,
            d=None, freqd=None, kmax=1, kmaxplot=1, crit=crit, abseps=abseps,
            flagm0=True, flageigenvect=False, optimized_convergence=True,
            fixed_lmax_nmax=(4,4))
    tuneshift_most = result_dict['tuneshift_most']
    tuneshiftnx = result_dict['tuneshiftnx']

    np.testing.assert_array_equal(tuneshift_most.shape,(1, 1, 1, 1, 1, 1),
                                  err_msg="tuneshift_most has incorrect shape")
    np.testing.assert_array_equal(tuneshiftnx.shape,(1, 1, 1, 1, 1, 1, 1),
                                  err_msg="tuneshiftnx has incorrect shape")
    
    assert not np.any(np.isnan(tuneshift_most)), "{} nan found".format(len(np.any(np.isnan(tuneshiftnx))))
    assert not np.all(tuneshift_most==0.), "only zeros found"
    
    np.testing.assert_allclose(np.real(tuneshiftnx.squeeze()),np.real(tuneshift_most.squeeze()),
                               rtol=0.,atol=1e-15,err_msg="tuneshift_most and tuneshiftnx differ (real part)")
    np.testing.assert_allclose(np.imag(tuneshiftnx.squeeze()),np.imag(tuneshift_most.squeeze()),
                               rtol=0.,atol=1e-15,err_msg="tuneshift_most and tuneshiftnx differ (imag part)")
    
    for iNb,Nb in enumerate(Nbscan):
        if Nb in expected_dQs:
            expected_dQ = expected_dQs[Nb]
            #print(tuneshift_most[:,:,iNb,:,:,:].squeeze())
            #print(expected_dQ)
            np.testing.assert_allclose(np.real(tuneshift_most[:,:,iNb,:,:,:].squeeze()),np.real(expected_dQ),
                                       rtol=1e-3,atol=0.,
                                       err_msg="Nb={}: real(tuneshift_most) not as expected, diff.={}".format(
                                       Nb, np.real(tuneshift_most[:,:,iNb,:,:,:].squeeze())-np.real(expected_dQ)))
            np.testing.assert_allclose(np.imag(tuneshift_most[:,:,iNb,:,:,:].squeeze()),np.imag(expected_dQ),
                                       rtol=1e-2,atol=0,
                                       err_msg="Nb={}: imag(tuneshift_most) not as expected, diff.={}".format(
                                       Nb, np.imag(tuneshift_most[:,:,iNb,:,:,:].squeeze())-np.imag(expected_dQ)))

print("*"*50)
print("LHC BB TMCI test: elapsed time = {} seconds".format(time.time()-t0))
print("*"*50)

#######################################################
# fifth test: LHC BB with M=1, checking full matrices
# (include case with realistic, Karliner-Popov damper)
#######################################################

machine = 'LHC'

# parameter definitions
plane = 'x'
dphase = 0.
Mscan = [1,3564]

nx = 0
Qx = 62.3099997613612
tunefrac = Qx-np.floor(Qx)
Qp = 5

# machine and beam parameters
gamma=6927.62806135325
beta=np.sqrt(1.-1./(gamma**2))
omega0=70657.55656577148

# Broad-band impedance
Rt = 25e6 # shunt impedance in MOhm/m
fr = 0.8e9 # cutoff frequency in Hz
Q = 1 # quality factor
freq = np.sort(list(set(np.hstack((np.logspace(1,13,121),np.linspace(5e8,5e9,201))))))
Z = resonator_impedance(Rt,fr,Q,freq)

# longitudinal parameters
taub = 1e-9 # bunch length (sec)
typelong = 'Gaussian' # Longitudinal distribution
eta = 0.0003479538899641484 # slip factor
omegas = 134.87527592319398
omegaksi = Qp*omega0/eta

t0 = time.time()
# Longitudinal distribution decomposition
g, a, b = longdistribution_decomp(taub, typelong=typelong)
#print("g[0]={}, a={}, b={}".format(g[0],a,b))

lmax,nmax = 6,16 # large numbers here (to check for possible overflows)

# Karliner-Popov damper
fd=np.concatenate((np.arange(2.e4,2.002e7,2.e4),np.arange(2.1e7,1.001e9,1e6),np.arange(1.1e9,1.01e10,1.e8)))
fd=np.concatenate((np.flipud(-fd),np.array([0.]),fd))
Zd=np.zeros((len(fd),2))
#print fd,len(fd)
L0=0.4
L1=0.1
L2=0.1
tauf=800*L1/299792458
dimp=damper_imp_Karliner_Popov(L0,L1,L2,tauf,R,Qx,f0,fd)
Zd[:,0]=np.real(dimp)
Zd[:,1]=np.imag(dimp)

for M in Mscan:
    # expected matrices from 2022 LHC BB calculations by N. Mounet on lxplus,
    # using the ctypes/C++ version and this script.
    matZ_expected = np.load("LHC_BB_test_matZ_{}b_output.npy".format(M))
    mat_ideal_damper_expected = np.load("LHC_BB_test_mat_ideal_damper_{}b_output.npy".format(M))
    mat_KP_damper_expected = np.load("LHC_BB_test_mat_KP_damper_{}b_output.npy".format(M))

    # compute impedance matrix
    matZ,max_freq = compute_impedance_matrix(
                lmax, nmax, nx, M, omegaksi, omega0, tunefrac,
                a, b, taub, g, Z, freq, flag_trapz=0, abseps=abseps,
                lmaxold=-1, nmaxold=-1, couplold=None)
    mat_ideal_damper = compute_damper_matrix(lmax,nmax,nx,M,omegaksi,omega0,
            tunefrac,a,b,taub,g,flagdamperimp=0,d=None,freqd=None,abseps=abseps,
            lmaxold=-1,nmaxold=-1,damperold=None)
    mat_KP_damper = compute_damper_matrix(lmax,nmax,nx,M,omegaksi,omega0,
            tunefrac,a,b,taub,g,flagdamperimp=1,d=Zd,freqd=fd,abseps=abseps,
            lmaxold=-1,nmaxold=-1,damperold=None)

    #np.save("LHC_BB_test_matZ_{}b_output.npy".format(M),matZ)
    #np.save("LHC_BB_test_mat_ideal_damper_{}b_output.npy".format(M),mat_ideal_damper)
    #np.save("LHC_BB_test_mat_KP_damper_{}b_output.npy".format(M),mat_KP_damper)
    
    np.testing.assert_array_equal(matZ.shape,(2*lmax+1, nmax+1, 2*lmax+1, nmax+1),
                                  err_msg="imp. matrix has incorrect shape (M={})".format(M))
    np.testing.assert_array_equal(mat_ideal_damper.shape,(2*lmax+1, nmax+1, 2*lmax+1, nmax+1),
                                  err_msg="ideal damper matrix has incorrect shape (M={})".format(M))
    np.testing.assert_array_equal(mat_KP_damper.shape,(2*lmax+1, nmax+1, 2*lmax+1, nmax+1),
                                  err_msg="KP damper matrix has incorrect shape (M={})".format(M))
    
    assert not np.any(np.isnan(matZ)) and not np.any(np.isinf(matZ)), \
        "{} nan and {} inf found in imp. matrix (M={})".format(
                np.sum(np.isnan(matZ)),np.sum(np.isinf(matZ)),M)
    assert not np.any(np.isnan(mat_ideal_damper)) and not np.any(np.isinf(mat_ideal_damper)),\
        "{} nan and {} inf found in ideal damper matrix (M={})".format(
                np.sum(np.isnan(mat_ideal_damper)),np.sum(np.isinf(mat_ideal_damper)),M)
    assert not np.any(np.isnan(mat_KP_damper)) and not np.any(np.isinf(mat_KP_damper)),\
        "{} nan and {} inf found in KP damper matrix (M={})".format(
                np.sum(np.isnan(mat_KP_damper)),np.sum(np.isinf(mat_KP_damper)),M)
    assert not np.all(matZ==0.), "only zeros found in imp. matrix (M={})".format(M)
    assert not np.all(mat_ideal_damper==0.), "only zeros found in ideal damper matrix (M={})".format(M)
    assert not np.all(mat_KP_damper==0.), "only zeros found in KP damper matrix (M={})".format(M)
    
    np.testing.assert_allclose(matZ,matZ_expected,rtol=1e-9,atol=0,
        err_msg="matZ and matZ_expected differ by {}".format(
        np.max(np.abs((matZ-matZ_expected)/matZ_expected))))
    np.testing.assert_allclose(mat_ideal_damper,mat_ideal_damper_expected,rtol=1e-12,atol=0,
        err_msg="mat_ideal_damper and mat_ideal_damper_expected differ by {}".format(
        np.max(np.abs((mat_ideal_damper-mat_ideal_damper_expected)/mat_ideal_damper_expected))))
    np.testing.assert_allclose(mat_KP_damper,mat_KP_damper_expected,rtol=1e-12,atol=0,
        err_msg="mat_KP_damper and mat_KP_damper_expected differ by {}".format(
        np.max(np.abs((mat_KP_damper-mat_KP_damper_expected)/mat_KP_damper_expected))))
    

print("LHC BB matrix test: elapsed time = {} seconds".format(time.time()-t0))
print("*"*50)

##################################################################
# sixth test: LHC BB in multibunch at low intensity, with chroma
# scan and computation of eta0 (sensitivity to dipole noise)
##################################################################

machine = 'LHC'

# parameter definitions
plane = 'x'
dphase = 0.
M = 11
Nb = 1e8
dampscan = [0.]

nxscan = np.arange(M)
Qx = 62.3099999992371
Qpscan = [-5, 10]
kmaxplot = 60
# to determine azimuthal mode number l, one allows also positive tuneshifts
# up to the following fraction of Qs:
fraction_of_Qs_allowed_on_positive_side = 0.05

# machine and beam parameters
circ = 26658.8831999989
R = circ/(2.*np.pi) # machine radius
gamma = 7460.52252761119
beta=np.sqrt(1.-1./(gamma**2))
f0=clight*beta/circ # rev. frequency
omega0=2.*np.pi*f0

# optics
alphap = 0.000346334442089481
#print("gamma={}, frev={}, alphap={}".format(gamma,f0,alphap))

# Broad-band impedance
Rt = 25e6 # shunt impedance in MOhm/m
fr = 5e9 # cutoff frequency in Hz
Q = 1 # quality factor
freq = np.sort(list(set(np.hstack((np.logspace(1,13,121),np.linspace(5e8,5e9,201))))))
Z = resonator_impedance(Rt,fr,Q,freq)

# longitudinal parameters
taub = 1.2008307535006611e-09 # bunch length (sec)
sigmaz = taub*beta*clight/4. # RMS bunch length (m)
typelong = 'Gaussian' # Longitudinal distribution
eta = alphap-1./(gamma*gamma) # slip factor
omegas = 148.38086900107388
Qs = omegas/omega0

t0 = time.time()
#print("eta={}, sigmaz={}, omegas={}".format(eta,sigmaz,omegas))
# Longitudinal distribution decomposition
g, a, b = longdistribution_decomp(taub, typelong=typelong)

for Qp in Qpscan:
    # DELPHI launch
    result_dict = eigenmodesDELPHI_converged_scan(
            [Qp], nxscan, dampscan, [Nb], [omegas], [dphase], M,
            omega0, Qx, gamma, eta, a, b, taub, g, Z, freq,
            particle='proton', flagnorm=flagnorm, flag_trapz=0, flagdamperimp=0,
            d=None, freqd=None, kmax=1, kmaxplot=kmaxplot, crit=crit, abseps=abseps,
            flagm0=False, flageigenvect=True, flag_eta_dipole=True,
            flag_eta_crab=True, optimized_convergence=True,
            )
    tuneshift_most = result_dict['tuneshift_most']
    tuneshiftnx = result_dict['tuneshiftnx']
    nx_most = result_dict['nx_most']
    eigenvectors = result_dict['eigenvectors_most']
    eta_dipole = result_dict['eta_dipole']
    eta_crab = result_dict['eta_crab']

    np.testing.assert_array_equal(tuneshift_most.shape,(1, 1, 1, 1, 1, kmaxplot),
                                  err_msg="Qp={}: tuneshift_most has incorrect shape".format(Qp))
    np.testing.assert_array_equal(tuneshiftnx.shape,(1, len(nxscan), 1, 1, 1, 1, kmaxplot),
                                  err_msg="Qp={}: tuneshiftnx has incorrect shape".format(Qp))
    np.testing.assert_array_equal(nx_most.shape,(1, 1, 1, 1, 1, kmaxplot),
                                  err_msg="Qp={}: nx_most has incorrect shape".format(Qp))
    
    assert not np.any(np.isnan(tuneshiftnx)), "Qp={}: {} nan found".format(Qp,len(np.any(np.isnan(tuneshiftnx))))
    assert not np.all(tuneshiftnx==0.), "Qp={}: only zeros found".format(Qp)
    
    inxmin = [np.argmin(np.imag(tuneshiftnx[0,:,0,0,0,0,k])) for k in range(kmaxplot)]
    expected_nx_most = list(zip(inxmin,nxscan[inxmin]))
    
    np.testing.assert_array_equal(tuple(np.squeeze(nx_most)),expected_nx_most,
            err_msg="Qp={}: nx_most does not contain most unstable coupled-bunch mode".format(Qp))

    headtail_phase = Qp*sigmaz/(eta*R)
    for inx,nx in enumerate(nxscan):
        the_l = []
        for k in range(kmaxplot):
            dQ = np.real(tuneshiftnx[0,inx,0,0,0,0,k])
            l = int(np.ceil(dQ/Qs))
            if (l-np.real(dQ)/Qs)> 1-fraction_of_Qs_allowed_on_positive_side:
                l -= 1
            the_l.append(l)

        #the_l = [int(np.ceil(tuneshiftnx[0,inx,0,0,0,0,k].real/Qs))
        #         for k in range(kmaxplot)]
        expected_eta_dipole_sum = np.array([np.abs(ive(l,headtail_phase**2))
                                            for l in range(-1,2)])
        eta_dipole_sum = np.array([sum([eta_dipole[0,inx,0,0,0,0,k]**2
                                        for k in range(kmaxplot) if the_l[k]==l])
                                   for l in range(-1,2)])

        np.testing.assert_allclose(eta_dipole_sum,expected_eta_dipole_sum,
                                   rtol=0.,atol=1e-5,
                                   err_msg="Qp={}, nx={}: eta_dipole not as expected, diff.={}".format(
                                        Qp, nx, np.abs(eta_dipole_sum-expected_eta_dipole_sum)))

print("*"*50)
print("LHC BB eta test: elapsed time = {} seconds".format(time.time()-t0))

print("*"*50)
print("All tests passed with success.")
print("*"*50)
